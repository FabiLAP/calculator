public class OwnLinkedList<G> {

	//fields 
	public ListElement first = null;
	
	public OwnLinkedList() {
		// constructor
	}
	
	public boolean isEmpty() {
		//is the linkedlist empty		
		return first == null; 
	}
	
	public G getFirstElement() {
		// return first element
		return (G) this.first;
	} 
	
	public void add(G newElement) {
		// add a new element to the linked list
		
		if (isEmpty()) {
			this.first = new ListElement (newElement, null);
		} else {
			this.first = new ListElement (newElement, this.first);
		}
	}
	
	public G remove() {
		// remove the first element from the linked list
		this.first = (ListElement) this.first.getNext();
		
		return (G) this.first;
	}
}