/**
 * The main part of the calculator doing the calculations.
 * 
 * @author  David J. Barnes and Michael Kolling 
 * @version 2008.03.30
 */
public class CalcEngine
{
    // The calculator's state is maintained in four fields:    
    private int base;    
    private String infix = "";
    private String result = "";    
    private OwnStack<String> infixHistory;

    /**
     * Create a CalcEngine. - standard constructor 
     */
    public CalcEngine()
    {
        clear();
        this.base=16;
        infixHistory = new OwnStack<String>();
    }
    /**
     * Create a CalcEngine. - needs a basis 
     */
    public CalcEngine(int base)
    {
    	clear();
    	this.base=base;
        infixHistory = new OwnStack<String>();
    }
    
//    public CalcEngine(int base, String oldvalue)
//    {
//    	clear();
//    	this.base=base;
//    	this.result=oldvalue;
//    }
    
    /*
     * getter for base
     */
    public int getBase(){
    	return this.base;
    }
    
	public String getInfixValue(){
		return infix;
    }
	
	public String getResultValue(){
		return result;
	}
	/*
	 * gets called by "=". Takes care of dates, changed the infixterm to 
	 * a postfix term and evaluates this for a given base
	 */
	public void calculateTheInfix() throws Exception{ 
		String processedInfix = processDate(infix);
		result = ""+evaluate(infixToPostfix(processedInfix));
		//for showing the result in the right base
		result = Integer.toString(Integer.parseInt(result), base);
  	}
	/*
	 * This methods is the same as calculateTheInfix but changes the output 
	 * to a gregorian date and adds the weekday od the day
	 */
  	public void calculateTheInfixInDate() throws Exception{ 
		String cache = processDate(infix);
		int resultAsInt = evaluate(infixToPostfix(cache));
  		result=JulianDate.toStingInGregorian(resultAsInt);
  		
  		result+=" ("+ JulianDate.getWeekday(resultAsInt) +")"; 
  	}
  	
  	//The method for incoming numbers, letters and operators
  	public void addToInfix(String incoming){
  		infix+=incoming;
  		infixHistory.push(infix);
  	}
  	    
    public void clear(){
        infix="";
        result="";
    }
    /*
     * method for stepping back in the history of the infix field
     */
    public void oneStepBack() throws Exception{
    	try {
			infixHistory.pop();
			infix=infixHistory.top();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("cant go back!");
		}
    }
    
    public void doTheFactorial() throws Exception{
    	result = Integer.toString(factorial(evaluate(infixToPostfix(infix))),base);
    }
    
    private int factorial(int n) {
        int cache = 1; 
        for (int i = 1; i <= n; i++) {
        	cache *= i;
        }
        return cache;
    }
    
	public String getTitle(){
        return "Java Calculator";
    }
   
    public String getAuthor(){
        return "Kai Schulz & Fabian B�nsch - adapted from David J. Barnes and Michael Kolling";
    }

	public String getVersion(){
       return "Version 1.0";
    }
        
	/*
	 * This method evaluates a given postfix String. It takes care of multidigits, depends to a given base
	 */
    public int evaluate (String pfx) throws Exception{
		int toReturn=0;
		//the Stack for the Operands - as Integers 
		OwnStack<Integer> operandStorage = new OwnStack<Integer>();
		
		//saving the String in a CharArray to have access to the chars with a normal loop
		char[] toEvaluate = pfx.toCharArray();
		int operandCache = 0;
		for (int i=0; i<toEvaluate.length; i++){
//			System.out.println("current char "+toEvaluate[i]);
			if (isOperand(toEvaluate[i])){
//				System.out.println("we got an operand");
				//####################################################### multidigit
				try{			
					if(isOperand(toEvaluate[i+1])){
//						System.out.println("next char was an operator too");
						operandCache=base*operandCache+Character.getNumericValue(toEvaluate[i]);
						//depends to the base!!!#################### yeah 
//						System.out.println("increases cache to: "+operandCache);
					}
					if(!isOperand(toEvaluate[i+1])){
//						System.out.println("next char wasnt an operator");
						operandCache=base*operandCache+Character.getNumericValue(toEvaluate[i]);
						//depends to the base!!!#################### yeah 
//						System.out.println("increases cache to: "+operandCache);
						operandStorage.push(operandCache);
//						System.out.println("cache gets pushed" + operandCache);
						operandCache=0;
//						System.out.println("reseted the cache");
					}
				}catch(Exception e){
//					 index out of bound exception. We have to check the last char
//					System.out.println("index out of bound exception - check last char");
					 if(operandCache==0){
						 operandStorage.push(Character.getNumericValue(toEvaluate[i]));
					 }else{
						 operandCache=base*operandCache+Character.getNumericValue(toEvaluate[i]);
						 operandStorage.push(operandCache);
					 }
				 }
				//####################################################### multidigit end				
				//Character.getNumericValue because a normal (int) casting doesn't work - ASCII
			}
			if (isOperator(toEvaluate[i])){
				//init
				int operand2=0;
				int operand1=0;
				//stack could throw exception, stackunderflow!
				try {
					operand2=operandStorage.top();
//					System.out.println("popped: "+operandStorage.top());
					operandStorage.pop();
					operand1=operandStorage.top();
//					System.out.println("popped: "+operandStorage.top());
					operandStorage.pop();
				} catch (Exception e) {
					// TODO Auto-generated catch block
//					System.out.println("ooooops");					
					throw new Exception("There are more operators than operands!!");
				}				
				int result = doASimpleCalc(operand1,toEvaluate[i],operand2);
//				System.out.println("pushed result: "+result);
				operandStorage.push(result);
			}
		}		
		try {
			toReturn = operandStorage.top();
			operandStorage.pop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Ohhh, there is no result. Was there Input?");
		}
		if (!operandStorage.isEmpty()){
			throw new Exception("There are more operands than operators!!");
		}
		return toReturn;
	}

	/*
	 * returns true if the parameter is a mathematical operand (letters for a base over 10 too)
	 */
	private boolean isOperand(char toCheck){
		if(toCheck == '0' || toCheck == '1' || toCheck == '2' || toCheck == '3' || toCheck == '4' ||
		   toCheck == '5' || toCheck == '6' || toCheck == '7' || toCheck == '8' || toCheck == '9' ||
		   (int)toCheck>=65 && (int)toCheck<=90 || (int)toCheck>=97 && (int)toCheck<=122
				) {
			return true;
		}
		else{
			return false;
		}
	}
	
	/*
	 * returns true if the parameter is a mathematical operator
	 */
	private boolean isOperator(char toCheck){
		if(toCheck == '+' || toCheck == '-' || toCheck == '*' || toCheck == '/' || toCheck == '^' || toCheck == '%'
				|| toCheck == '(' || toCheck == ')'
				){
			return true;
		}
		else{
			return false;
		}
	}
	/*
	 * for evaluating simple calculations with lefthand, righthand operand and a operator
	 */
	private int doASimpleCalc(int operand1, char operator, int operand2){		
		if(operator=='+'){			
			return operand1+operand2;
		}
		if(operator=='-'){
			return operand1-operand2;
		}
		if(operator=='*'){
			return operand1*operand2;
		}
		if(operator=='/'){
			return operand1/operand2;
		}
		if(operator=='^'){
			return (int)Math.pow(operand1, operand2);
		}
		if(operator=='%'){
			return operand1%operand2;
		}
		return 0;
	}
	/*
	 * This method changes infix Strings to postfix Strings. It does take care of multidigits and paranthesis
	 */
	 public String infixToPostfix (String ifx) throws Exception{
		 char[] infix = ifx.toCharArray();
		 OwnStack<Character> opperatorStorage=new OwnStack<Character>();
		 String postfix = "";
		 String operandCache = "";
		 for (int i=0; i<infix.length; i++){
			 //If its an operand - put it to the output String
//			 System.out.println("current Char: "+infix[i]);
			 if(isOperand(infix[i])){
				//################################################### multidigit
				 try{
					 if(isOperand(infix[i+1])){
						 operandCache=operandCache+infix[i];
					 }				 
					 if(!isOperand(infix[i+1])){
						 operandCache=operandCache+infix[i];
						 postfix+=operandCache+" ";
						 operandCache="";
					 }
				 }catch(Exception e){
					 //index out of bound exception. We have to check the last char
					 if(operandCache.equals("")){
						 postfix+=infix[i]+" ";
					 }else{
						 postfix+=operandCache+infix[i]+" ";
					 }
				 }
				//################################################### multidigit end
//				 System.out.println("added to Postfix :"+infix[i]);
			 }
			 //if its an operator
			 else if(isOperator(infix[i])){
//				 System.out.println("Was an Operator: "+infix[i]);
				 // and if the stack is empty - push
				 if(opperatorStorage.isEmpty()){
					 opperatorStorage.push(infix[i]);
//					 System.out.println("Pushed to Stack: "+infix[i]);
				 }
				 //################################# Parenthesis
				 else if(infix[i]=='('){
					 opperatorStorage.push(infix[i]);
//					 System.out.println("Pushed to Stack: "+infix[i]);
				 }
				 else if (infix[i]==')'){
					 try {
						while (opperatorStorage.top()!='('){
							postfix+=opperatorStorage.top()+" ";
//							System.out.println("added Op from Stack to Postfix cause ')': "+opperatorStorage.top());
							opperatorStorage.pop();	
//							System.out.println("Popped");
						 }
//						System.out.println("Stack top have to be be a (");
//						System.out.println("pop: "+opperatorStorage.top());
						opperatorStorage.pop();	
//						System.out.println("Popped");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						throw new Exception("Parathesis failure!");
					}
				 }
				 //################################# Parenthesis end
				 //if the stack isn't empty and the char wasn't a parenthesis
				 else{
					 try {
						 //as long the stack is not empty and the priority of the current operator is higher/equal the last ones
//						 System.out.println("COMPARE with current Stack: "+opperatorStorage.toString());
						while(!opperatorStorage.isEmpty() && getPriority(opperatorStorage.top())<=getPriority(infix[i])){
							//put the last one to the output String
							postfix+=opperatorStorage.top()+" ";
//							System.out.println("added Op from Stack to Postfix: "+opperatorStorage.top());
							 //and pop the last operator
							opperatorStorage.pop();	
//							System.out.println("Popped");
						}
						//if there is no more operator in the stack or a lesser priority - push current operator
						opperatorStorage.push(infix[i]);
//						System.out.println("Pushed to stack: "+infix[i]);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						throw new Exception("Seems to be an incorrect mathematical term, ckeck the operators!");
					}							 
				 }				 
			 }
			 else if(infix[i]==' '){
				 //do nothing
			 }
			 else{
				 //the char was either a operator nor an operand or whitespace - throw error
				 throw new Exception(infix[i]+" - This wasnt a right mathematical character!");
			 }
//			 System.out.println("## current postfix term is: "+postfix);
		 }	
//		 System.out.println("## Last Char reached - put the rest to Postfix");
		 //if there is no more character at the infix and there are still operators in the stack
		 while(!opperatorStorage.isEmpty()){
			//nach parenthesis checken und exception werfen lassen!
				if(opperatorStorage.top()==')' || opperatorStorage.top()=='('){
					throw new Exception("Parathesis failure!");
				}
			 try {				
				//put them to the output String
				postfix+=opperatorStorage.top()+" ";
//				System.out.println("added to postfix :"+opperatorStorage.top());
				//and pop them
				opperatorStorage.pop();					
			} catch (Exception e) {
				// TODO Auto-generated catch block 
				throw new Exception("Seems to be an incorrect term or something else!!!!!!");
			}					
		 }
//		 System.out.println("## The end");
		 return postfix;
	 }
	 /*
	  * This methods returns the priority of a given operator
	  */
	 private int getPriority(char operator){
		 if(operator == '+' || operator == '-'){
			 return 4;
		 }
		 if(operator == '*' || operator == '/'|| operator == '%'){
			 return 3;
		 }
		 if(operator == '^'){
			 return 2;
		 }
		 //10, because of the parenthesis!
		 return 10;
	 }
	 /*
	  * This is a new method which detects gregorian dates in String which should be in a infix order
	  * The detected dates (see stating tag # and points as separators) gets transformed into juliandate
	  * Other parts of the string gets unmodified
	  */
	 public String processDate(String incomming){
		 String toReturn = "";
		 int day = 0;
		 int month = 0;
		 int year = 0;
		 int datestatus=0;  //0 - day; 1 - month; 2 - year
		 boolean buildingDate = false;
		 char[] chars = incomming.toCharArray();
		 for (int i=0; i<chars.length; i++){
//			 System.out.println("###############################################");
//			 System.out.println("current char"+chars[i]);
			 if(chars[i]=='#'){ //this is the starting tag for a gregorian date
//				 System.out.println("hit a # - setting buildingdate and datestatus");
				 buildingDate = true;
				 datestatus = 0;
			 }
			 if(!buildingDate){ //if we are not in a date just skip and add to retrun string
//				 System.out.println("buildingdate is false and the currentchar was added to the return");
				 toReturn+=chars[i];
			 }
			 if(buildingDate){ //we are anywhere in the date
//				 System.out.println("buildingdate was true");
				 if(chars[i] == '.'){	//if its a point - switch the status (day to month to year)
//					 System.out.println("incremented datestatus");
					 datestatus++;
				 }
				 if(!isOperand(chars[i]) && chars[i]!='.' && chars[i]!='#'){ //the date is complete!!
//					 System.out.println("hit an operand and buildingdate was true");
					 buildingDate=false;
//					 System.out.println("set to false the buildingdate");
					 // convert to julian date and add to string!
					 int asJulian = JulianDate.toJulian(year, month, day);
//					 System.out.println("create the jd with: " + day + " " + month + " " + year);
					 toReturn+=asJulian;
//					 System.out.println("added "+asJulian+ " to the return string");
					 //reset the d m y
					 day = 0;
					 month = 0;
					 year = 0;					 
//					 System.out.println("reseted d m y");
					 //the char which breaks the date have to be printed
					 toReturn+=chars[i];
				 }
				 else{	//its a number
//					 System.out.println("was a number and buildingdate was true");
					 if(datestatus==0  && chars[i]!='.'  && chars[i]!='#'){ //increase days
//						 System.out.println("old value: "+ day);
//						 System.out.println("curent char in numeric : "+Character.getNumericValue(chars[i]));
						 day=base*day+Character.getNumericValue(chars[i]);
//						 System.out.println("increase days");
//						 System.out.println("increased days to: "+ day);
					 }
					 if(datestatus==1  && chars[i]!='.'  && chars[i]!='#'){ //increase months
//						 System.out.println("old value: "+ month);
//						 System.out.println("curent char in numeric : "+Character.getNumericValue(chars[i]));
						 month=base*month+Character.getNumericValue(chars[i]);
//						 System.out.println("increase month");
//						 System.out.println("increased days to: "+ month);
					 }
					 if(datestatus==2  && chars[i]!='.'  && chars[i]!='#'){ //increase years
//						 System.out.println("old value: "+ year);
//						 System.out.println("curent char in numeric : "+Character.getNumericValue(chars[i]));
						 year=base*year+Character.getNumericValue(chars[i]);
//						 System.out.println("increase years");
//						 System.out.println("increased days to: "+ year);
					 }
				 }				 
			 }
		 }
		 if(buildingDate){	//if the last char was a part of a date - this date wasnt converted to jd
			 toReturn+=JulianDate.toJulian(year, month, day);
		 }
		 return toReturn;
	    }
	 
	 /*
	  * just for testing. 
	  */
	 public String calculateTheInfixForTesting(String testinfix) throws Exception{ 
		String processedInfix = processDate(testinfix);	
		String testresult = ""+evaluate(infixToPostfix(processedInfix));
		//for showing the result in the right base
		testresult = Integer.toString(Integer.parseInt(testresult), base);
		return testresult.toUpperCase();
  	}
	 
	 /*
	  * just for testing
	  */
	 public String calculateTheInfixInDateForTesting(String testinfix) throws Exception{ 
		String cache = processDate(testinfix);
		int resultAsInt = evaluate(infixToPostfix(cache));
  		String testresult=JulianDate.toStingInGregorian(resultAsInt);
  		
  		testresult+=" ("+ JulianDate.getWeekday(resultAsInt) +")";
  		return testresult;
  	}
}