
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Testing {

	public static void main(String[] args) throws Exception {
		
		//#############################################
		
		//TODO evaluating of postfix expressions
		
		CalcEngine testingEngineDec = new CalcEngine(10);
		
		System.out.println("######################");
		System.out.println("1 result: "+testingEngineDec.evaluate("1 2 * 3 +"));
		System.out.println("######################");
		System.out.println("2 result: "+testingEngineDec.evaluate("1 2 3 * +"));
		System.out.println("######################");
		System.out.println("3 result: "+testingEngineDec.evaluate(" 1 2 + 3 4 ^ -"));
		System.out.println("######################");
		System.out.println("4 result: "+testingEngineDec.evaluate(" 1 2 ^ 3 4 * -"));
		System.out.println("######################");
		System.out.println("5 result: "+testingEngineDec.evaluate("1 2 3 * + 4 5 ^ - 6 +"));
		System.out.println("######################");
		System.out.println("6 result: "+testingEngineDec.evaluate(" 1 2 + 3 * 4 5 6 - ^ +"));
		System.out.println("######################");
		System.out.println("7 result: "+testingEngineDec.evaluate("1 2 + 3 4 / + 5 + 6 7 8 + * +"));
		System.out.println("######################");
		System.out.println("8 result: "+testingEngineDec.evaluate(" 9 1 - 2 - 3 2 * - 1 -"));
		System.out.println("######################");
		System.out.println();
		
		//###########################################
		System.out.println("### decimal" );
		System.out.println("Input was: "+"(11-2)*(14-10)");
		System.out.println("Result: "+testingEngineDec.calculateTheInfixForTesting("(11-2)*(14-10)"));
		System.out.println();
		
		CalcEngine engineHex = new CalcEngine(16);
		System.out.println("### hexadecimal" );
		System.out.println("Input was: "+"(BB-A)*(1F-11)");
		System.out.println("Result: "+engineHex.calculateTheInfixForTesting("(BB-A)*(1F-11)"));
		System.out.println();
		//###########################################
		
		//TODO infix to postfix testings
		
		String ex1 = "133*2+3";
		String ex2 = "1+232*3";
		String ex3 = "1+2-323^4";
		String ex4 = "1^223-3*4";
		String ex5 = "1+222*3-4^5+6";
		String ex6 = "(1+234)*3+(435^(5-6))";
		String ex7 = "1+2+3/4+5+356*(7+8)";
		String ex8 = "94-1-22-3*222-1";
		
		System.out.println("######################");
		System.out.println("pfix n1:  "+testingEngineDec.infixToPostfix(ex1));
//		System.out.println("should be 1 2 * 3 + ");
		System.out.println("######################");
		System.out.println("pfix n2:  "+testingEngineDec.infixToPostfix(ex2));
//		System.out.println("should be 1 2 3 * + ");
		System.out.println("######################");
		System.out.println("pfix n3:  "+testingEngineDec.infixToPostfix(ex3));
//		System.out.println("should be 1 2 + 3 4 ^ - ");
		System.out.println("######################");
		System.out.println("pfix n4:  "+testingEngineDec.infixToPostfix(ex4));
//		System.out.println("should be 1 2 ^ 3 4 * - ");
		System.out.println("######################");
		System.out.println("pfix n5:  "+testingEngineDec.infixToPostfix(ex5));
//		System.out.println("should be 1 2 3 * + 4 5 ^ - 6 + ");
		System.out.println("######################");
		System.out.println("pfix n6:  "+testingEngineDec.infixToPostfix(ex6));
//		System.out.println("should be 1 2 + 3 * 4 5 6 - ^ + ");
		System.out.println("######################");
		System.out.println("pfix n7:  "+testingEngineDec.infixToPostfix(ex7));
//		System.out.println("should be 1 2 + 3 4 / + 5 + 6 7 8 + * + ");
		System.out.println("######################");
		System.out.println("pfix n8:  "+testingEngineDec.infixToPostfix(ex8));
//		System.out.println("should be 9 1 - 2 - 3 2 * - 1 - ");
		System.out.println("######################");
		
//		//###########################################
		
		//TODO Testings for * and +
		
		//uses calculateTheInfixForTesting() method
		
		String[] input = {
				"1*2+3",
				"1+2*3",
				"1+2-3^4",
				"1^2-3*4",
				"1+2*3-4^5+6",
				"(1+2)*3+(4^(5-6))",
				"1+2+3/4+5+6*(7+8)",
				"9-1-2-3*2-1",
				"2*6*2+1",
				"(2+1)^2-1",
				"11%4+1",
				"11%4*4",
				"(2+1)*((3-1)/2+1)",
				"32*12/2+45+9%2-3",
				"400%100",
				"9+2",
				"BB+AA3",
				"FFFFFF-FF",
				"C*F",
				"10-FF"
		};
		String[] expectedOutput = {
				 "5",
				 "7",
				 "-78",
				 "-11",
				 "-1011",
				 "9",
				 "98",
				 "-1",
				 "25",
				 "8",
				 "4",
				 "12",
				 "6",
				 "235",
				 "0",
				 "B",
				 "B5E",
				 "FFFF00",
				 "B4",
				 "-EF"
				
				 
		};
		
		System.out.println();
		System.out.println("############################");
		System.out.println("Evaluation Testing");
		
		for(int i=0; i<15;i++){
			System.out.println("############################");
			System.out.println("Test Nr. "+(i+1));
			System.out.println("Input was: "+input[i]);
			System.out.println("calculated result : "+testingEngineDec.calculateTheInfixForTesting(input[i]));
			System.out.println("expected result   : "+expectedOutput[i]);
		}
		
		System.out.println("############################");
		System.out.println("Switched to HEX");
		
		CalcEngine testingEngineHex = new CalcEngine(16);
		for(int i=15; i<input.length;i++){
			System.out.println("############################");
			System.out.println("Test Nr. "+(i+1));
			System.out.println("Input was: "+input[i]);
			System.out.println("calculated result : "+testingEngineHex.calculateTheInfixForTesting(input[i]));
			System.out.println("expected result   : "+expectedOutput[i]);
		}			
		System.out.println("");
		
//		//###########################################
		
//		//###########################################
		
		//TODO Testings for exceptions
		
		//uses calculateTheInfixForTesting() method and surround each test with try-catch block
		
		String[] inputException = {
			"55+",
			"25+(2*4))",	
			"25+((2*4)",
			"",
			"(5+5)5",
			"5+5+#",
			"(4)(4)",
			"(.)(.)",
			"(.Y.)"
		};
		
		String[] thrownExceptionExpected = {
			"There are more operators than operands!!",
			"Parathesis failure!",
			"Parathesis failure!",
			"Ohhh, there is no result. Was there Input?",
			"There are more operands than operators!!",
			"- This wasnt a right mathematical character!",
			"There are more operands than operators!!",
			"",
			""			
		};
		System.out.println("############################");
		System.out.println("Exception Testing");
		
		for (int i = 0; i < inputException.length; i++) {
			try {
				testingEngineDec.calculateTheInfixForTesting(inputException[i]);
			} catch (Exception e) {
				System.out.println("############################");
				System.out.println("Exception Test Nr. "+(i + 1));
				System.out.println("TestString: " + inputException[i]);
				System.out.println("Expectet Exception: " + thrownExceptionExpected[i]);
				System.out.println("Thrown Exception:   " + e.getMessage());
			}
		}
		
		String[] datetests = { 	"23+#12.1.2013+23",
								"#14.12.2013 + #15.12.2013",
								"23+#12.1.2013"
		};
		
		System.out.println("");
		System.out.println("############################");
		System.out.println("JDconversion test");
		
		for (int i = 0; i < datetests.length; i++) {
			System.out.println("############################");    
			System.out.println("input was: "+datetests[i]);
			System.out.println("output is: "+testingEngineDec.processDate(datetests[i]));
		}
		
//		//###########################################
		
//		BufferedReader userin = new BufferedReader(new InputStreamReader(System.in));
//		String userInput;
//		try {
//			while ((userInput = userin.readLine()) != null) {
//
//				if(userInput.equals("bye")||userInput.equals("exit")){
//					System.out.println("Im feeling good");
//					System.exit(0);
//				}
//				System.out.println(trial.evaluate(trial.infixToPostfix(userInput)));
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		//###########################################			
	}
}