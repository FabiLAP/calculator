
public class OwnStack<G> {
	
	//fields
	private OwnLinkedList<G> list;
	
	public OwnStack() {
		// constructor
		list = new OwnLinkedList<G>();
	}
	
	public boolean isEmpty() {
		//is the stack empty
		return list.isEmpty();
	}
	
	public void push(G newElement){
			list.add(newElement);
	}
	
	public G pop() throws Exception {
		// get first element of the stack and remove
		if (this.isEmpty()) {
			throw new Exception("empty");
		} else { 
			return list.remove();
		}
	}

	public G top() throws Exception {
//		 have a look at the first element
		if (this.isEmpty()) {
			throw new Exception("empty");
		} else { 
		return (G) list.first.getValue();
		}
	}
	
	public void clear() {
		// delete all elements of the stack
		this.list = new OwnLinkedList<G>();
	}
	
	public String printAll() {
		//all elements of stack to string
		
		String output = "";
		//generate output string
		ListElement it = new ListElement ( "it", null);
		it = (ListElement) list.getFirstElement();
		
		while (it != null) {
			output += it.getValue() + " | ";
			it = (ListElement) it.getNext();
		} 		
		return output;	
	} 
}