/**
 * The main class of a simple calculator. Create one of these and you'll
 * get the calculator on screen.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2008.03.30
 */
public class Calculator
{
    private CalcEngine engine;
    private UserInterface gui;

    /**
     * Create a new calculator and show it.
     * Here we have the base as parameter - should be delivered as a String from the main
     */
    public Calculator(String base)
    {
    	engine = new CalcEngine(Integer.parseInt(base)); 	
        gui = new UserInterface(engine);
    }
    /*
     * the old standard constructor
     */
    public Calculator()
    {
    	engine = new CalcEngine(); 	
        gui = new UserInterface(engine);
    }
    
    /**
     * In case the window was closed, show it again.
     */
    public void show()
    {
        gui.setVisible(true);
    }
    /*
     * the mainmethod
     * here we differentiate if there was passed an argument
     * if yes, generate a new calculatorengine with base of 
     */
    public static void main(String[] args) {
    	if (args.length!=0){
	    	Calculator cal = new Calculator(args[0]);
	    }    
    	else{
    		Calculator cal = new Calculator();
    	}
    }
}