import java.awt.*;
import java.awt.event.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.*;
import javax.swing.border.*;

/**
 * A graphical user interface for the calculator. No calculation is being
 * done here. This class is responsible just for putting up the display on 
 * screen. It then refers to the "CalcEngine" to do all the real work.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2008.03.30
 */
public class UserInterface
    implements ActionListener{
    private CalcEngine calc;
    private boolean showingAuthor;
    private JFrame frame;
    private JTextField infixDisplay;
    private JTextField resultDisplay;
    private JPanel panelcheckboxes;
    private JPanel buttonPannel;
    //saves the buttons from the buttonpanel to this array - so accessible
    private JButton[] buttonAccesArray;    
    private JLabel status;
    private JPanel contentPane =null;
    private JPanel opperatorPanel=null;
    private JPanel opperatorPanelNumeric=null;
    private JPanel opperatorPanelDate=null;
    /**
     * Create a user interface.
     * @param engine The calculator engine.
     */
    public UserInterface(CalcEngine engine)    {
        calc = engine;
        showingAuthor = true;
        makeFrame();
        frame.setVisible(true);
    }

    /**
     * Set the visibility of the interface.
     * @param visible true if the interface is to be made visible, false otherwise.
     */
    public void setVisible(boolean visible){
        frame.setVisible(visible);
    }
    
   /*
    * This method returns a panel as needed to the given base
    * Produces Buttons corresponding to the base
    * also named the buttons in the right order 1,2,..,8,9,A,B,C,..
    * in addition it saves the particular buttons in the buttonAccesArray
    */
    public JPanel generatePanel(int base){
    	JPanel toReturn = new JPanel(new GridLayout(4, 4));
    	buttonAccesArray = new JButton[base];
    	for(int i=0; i<base;i++){
    		//############################################################################# max 36er basis
    		String currentIasString = Integer.toString(i, base).toUpperCase();    		   		
    		JButton button = new JButton(currentIasString);
            button.addActionListener(this);
            toReturn.add(button);
            //saves the created button to the buttonAccesArray to the corresponding Array-entry
            buttonAccesArray[i]=button;            
    	}    	
    	return toReturn;
    }
    /*
     * This method sets the buttons from the buttonAccesArray visible or invisible corresponding to the base
     */
    private void makeButtonsVisible(int base){
    	for (int i = 0; i<base;i++){
    		buttonAccesArray[i].setVisible(true);    		
    	}
    	for (int i = base; i<buttonAccesArray.length;i++){
    		buttonAccesArray[i].setVisible(false);    		
    	}
    }
    /**
     * Make the frame for the user interface.
     */
    private void makeFrame(){
        frame = new JFrame(calc.getTitle());
        
        contentPane = (JPanel)frame.getContentPane();
        contentPane.setLayout(new BorderLayout(8, 8));
        contentPane.setBorder(new EmptyBorder( 10, 10, 10, 10));

        infixDisplay = new JTextField();
        //contentPane.add(display, BorderLayout.NORTH);
        
        resultDisplay = new JTextField();
        //contentPane.add(display2, BorderLayout.PAGE_END);
        
        JPanel displays=new JPanel(new GridLayout(2,1));
        displays.add(infixDisplay);
        displays.add(resultDisplay);
        contentPane.add(displays, BorderLayout.NORTH);

        opperatorPanelNumeric = new JPanel(new GridLayout(7, 2));
        
	        addButton(opperatorPanelNumeric, "(");
	        addButton(opperatorPanelNumeric, ")");  
        
        	addButton(opperatorPanelNumeric, "+");
	        addButton(opperatorPanelNumeric, "-");
	        
	        addButton(opperatorPanelNumeric, "*");        
	        addButton(opperatorPanelNumeric, "/");
        
            addButton(opperatorPanelNumeric, "^");
            addButton(opperatorPanelNumeric, "%");
            
            addButton(opperatorPanelNumeric, "!");
            addButton(opperatorPanelNumeric, "=");  
            
            addButton(opperatorPanelNumeric, "CL");
            addButton(opperatorPanelNumeric, "?");
            
            JButton panicButton = new JButton("PANIC!");
            panicButton.addActionListener(this);
            panicButton.setForeground(Color.red);
            opperatorPanelNumeric.add(panicButton);
            
            addButton(opperatorPanelNumeric, "back");
            
        opperatorPanelDate = new JPanel(new GridLayout(4, 2));
            
	        addButton(opperatorPanelDate, "#");
	        opperatorPanelDate.add(new JLabel(" "));
	        addButton(opperatorPanelDate, ".");  
	        opperatorPanelDate.add(new JLabel(" "));        
        	addButton(opperatorPanelDate, "=(date)");
        	opperatorPanelDate.add(new JLabel(" "));
	        addButton(opperatorPanelDate, "today");
	        opperatorPanelDate.add(new JLabel(" "));
	        
        opperatorPanel = new JPanel(new GridLayout(1, 2));
        opperatorPanel.add(opperatorPanelNumeric);
        opperatorPanel.add(opperatorPanelDate);
	        
              
        contentPane.add(opperatorPanel, BorderLayout.WEST);
        opperatorPanelDate.setVisible(false);
        
        buttonPannel = generatePanel(calc.getBase());
        contentPane.add(buttonPannel, BorderLayout.CENTER);
        
        
        status = new JLabel(calc.getAuthor());
        contentPane.add(status, BorderLayout.SOUTH);        
        
        //#############################################        
        panelcheckboxes = new JPanel(new GridLayout(6, 1));
        
        //F�nf RadioButtons werden erstellt
        JRadioButton  hex = new JRadioButton ("Hex");
        JRadioButton  dez = new JRadioButton ("Dec");
        JRadioButton  oct = new JRadioButton ("Oct");
        JRadioButton  bin = new JRadioButton ("Bin");
        JRadioButton  args = new JRadioButton ("Args",true);
        JRadioButton  date = new JRadioButton ("Date");

        //gruppieren um mehrfachaswahlen zu verhindern
        ButtonGroup group = new ButtonGroup();
        group.add(hex);
        group.add(dez);
        group.add(oct);
        group.add(bin);
        group.add(args);
        group.add(date);
 
        //Buttons werden dem Listener zugeordnet
        hex.addActionListener(this);
        dez.addActionListener(this);
        oct.addActionListener(this);
        bin.addActionListener(this);
        args.addActionListener(this);
        date.addActionListener(this);
 
        //Buttons werden dem JPanel hinzugef�gt
        panelcheckboxes.add(hex);
        panelcheckboxes.add(dez);
        panelcheckboxes.add(oct);
        panelcheckboxes.add(bin);
        panelcheckboxes.add(args);
        panelcheckboxes.add(date);
 
        contentPane.add(panelcheckboxes, BorderLayout.EAST);        
  
        frame.pack();
    }

    /**
     * Add a button to the button panel.
     * @param panel The panel to receive the button.
     * @param buttonText The text for the button.
     */
    private void addButton(Container panel, String buttonText){
        JButton button = new JButton(buttonText);
        button.addActionListener(this);
        panel.add(button);
     }

    /**
     * An interface action has been performed.
     * Find out what it was and handle it
     * @param event The event that has occurred.
     */
    public void actionPerformed(ActionEvent event){
        String command = event.getActionCommand();
        
        if(command.equals("=")) {
           	try {
				calc.calculateTheInfix();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//System.out.println(e.getMessage());
				JOptionPane.showMessageDialog(frame,
						e.getMessage());
			}
        }
        else if(command.equals("=(date)")) {
           	try {
			calc.calculateTheInfixInDate();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//System.out.println(e.getMessage());
				JOptionPane.showMessageDialog(frame,
						e.getMessage());
			}
        }
        else if(command.equals("today")) {
        	Calendar actueldate = new GregorianCalendar();
    		int gyear = actueldate.get(Calendar.YEAR);
    		int gmonth = actueldate.get(Calendar.MONTH) + 1;          
    		int gdate = actueldate.get(Calendar.DATE);
        	calc.addToInfix("#"+gdate+"."+gmonth+"."+gyear);
        }
        else if(command.equals("back")) {
           try {
   				calc.oneStepBack();
   			} catch (Exception e) {
   				// TODO Auto-generated catch block
   				//System.out.println(e.getMessage());
   				JOptionPane.showMessageDialog(frame,
						e.getMessage());
   			}
        }        
        else if(command.equals("CL")) {
            calc.clear();
        }
        else if(command.equals("!")) {
            try {
				calc.doTheFactorial();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//System.out.println(e.getMessage());
				JOptionPane.showMessageDialog(frame,
						e.getMessage());
			}
        }
        else if(command.equals("?")) {
            showInfo();
        }
        //#######################################
        // assign a new calcengine to the field by passing the given base
        // thereafter make the the relevant buttons visible
        else if(command.equals("Hex")) {
        	this.calc=new CalcEngine(16);
        	makeButtonsVisible(16);
        	opperatorPanelDate.setVisible(false);
        }
        else if(command.equals("Dec")) {
        	this.calc=new CalcEngine(10);
        	makeButtonsVisible(10);
        	opperatorPanelDate.setVisible(false);
        }
        else if(command.equals("Oct")) {
        	this.calc=new CalcEngine(8);
        	makeButtonsVisible(8);
        	opperatorPanelDate.setVisible(false);
        }
        else if(command.equals("Bin")) {
        	this.calc=new CalcEngine(2);
        	makeButtonsVisible(2);
        	opperatorPanelDate.setVisible(false);
        }
        else if(command.equals("Args")) {
        	//Initialize a engine like the one which was created by the start of the UI
        	this.calc=new CalcEngine(buttonAccesArray.length);
        	makeButtonsVisible(buttonAccesArray.length);
        	opperatorPanelDate.setVisible(false);
        }
        else if(command.equals("Date")) {
        	this.calc=new CalcEngine(10);
        	makeButtonsVisible(10);
        	opperatorPanelDate.setVisible(true);
        	
        }        
        else if(command.equals("PANIC!")) {
        	System.exit(0);
        }
        else{
        	// all numbers/letters and other operators
	        calc.addToInfix(command);
	    }

        updateInfixDisplay();
        updateResultDisplay();
    }

    /**
     * Update the interface display to show the current value of the 
     * calculator.
     */
    private void updateInfixDisplay(){
        infixDisplay.setText("" + calc.getInfixValue().toUpperCase());
    }
    
    private void updateResultDisplay(){
        resultDisplay.setText("" + calc.getResultValue().toUpperCase());
    }

    /**
     * Toggle the info display in the calculator's status area between the
     * author and version information.
     */
    private void showInfo(){
        if(showingAuthor)status.setText(calc.getVersion());
        else
            status.setText(calc.getAuthor());

        showingAuthor = !showingAuthor;
    }
}