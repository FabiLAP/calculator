public class JulianDate{

	private int JdValue;
	
	public JulianDate(int JdValue) {
		// TODO Auto-generated constructor stub
		this.JdValue=JdValue;
	}
	
	public JulianDate (int year, int month, int day){
		this.JdValue=toJulian(year, month, day);
	}
	
	public int getDate (){
		return JdValue;
	}
	
	public static String getWeekday(int jd){
		if (jd%7 == 6){
			return "Sunday";
		} else if (jd%7 == 5) {
			return"Saturday";
		} else if (jd%7 == 4) {
			return "Friday";
		} else if (jd%7 == 3) {
			return"Thursday";
		} else if (jd%7 == 2) {
			return"Wednesday";
		} else if (jd%7 == 1) {
			return"Tuesday";
		} else if (jd%7 == 0) {
			return"Monday";
		}
		return "";
	}
	
	public static int[] getDateInGregorian(int JdValue){
		return fromJulian(JdValue);
	}
	
	public static String toStingInGregorian(int JdValue){
		int[] date = fromJulian(JdValue);
		return ""+date[2]+"."+date[1]+"."+date[0];
	}
	/*
	 * from http://www.rgagnon.com/javadetails/java-0506.html
	 */
	protected static int toJulian(int year, int month, int day){
		int julianYear = year;
		if (year < 0) julianYear++;
		int julianMonth = month;
		if (month > 2) {
			julianMonth++;
			}
		else {
			julianYear--;
			julianMonth += 13;
		}
		double julian = (Math.floor(365.25 * julianYear)
		   + Math.floor(30.6001*julianMonth) + day + 1720995.0);
		if (day + 31 * (month + 12 * year) >= 15 + 31*(10+12*1582)) {
			// change over to Gregorian calendar
			int ja = (int)(0.01 * julianYear);
			julian += 2 - ja + (0.25 * ja);
		}
		return (int) Math.floor(julian);
		}
	
	/*
	 * from http://www.rgagnon.com/javadetails/java-0506.html
	 */
	protected static int[] fromJulian(int injulian) {
		   int jalpha,ja,jb,jc,jd,je,year,month,day;
		   double julian = injulian + 0.5 / 86400.0;
		   ja = (int) julian;
		   if (ja>= 15 + 31*(10+12*1582)) {
		     jalpha = (int) (((ja - 1867216) - 0.25) / 36524.25);
		     ja = ja + 1 + jalpha - jalpha / 4;
		   }

		   jb = ja + 1524;
		   jc = (int) (6680.0 + ((jb - 2439870) - 122.1) / 365.25);
		   jd = 365 * jc + jc / 4;
		   je = (int) ((jb - jd) / 30.6001);
		   day = jb - jd - (int) (30.6001 * je);
		   month = je - 1;
		   if (month > 12) month = month - 12;
		   year = jc - 4715;
		   if (month > 2) year--;
		   if (year <= 0) year--;

		   return new int[] {year, month, day};
		  }
}