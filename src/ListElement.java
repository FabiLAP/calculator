
public class ListElement<G> {
	
	//fields
	private G value;
	private ListElement next = null;

	public ListElement(G value, ListElement next) {
		// TODO Auto-generated constructor stub
		this.value = value;
		this.next = next;
	}
	
	public G getValue() {
		return value;
	}

	public ListElement getNext() {
		return next;
	}
	
	public void setNext(ListElement setNext) {
		this.next = setNext;
	}
	
	public String toString() {
		// TODO Auto-generated method stub
		return (String) value;
	} 
}